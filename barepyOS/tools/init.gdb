# -*- mode:gdb-script -*-

layout split
focus cmd
winheight cmd 25
target remote:1234

set history filename ~/.gdb_history
set history save

b *reset_asm_handler
b *after_kmain
b kernel_panic

b *swi_handler

b *sys_settime
b *do_sys_settime

b *sys_gettime
b *do_sys_settime

b *sys_reboot
b *do_sys_reboot

b *sys_nop
b *do_sys_nop

b *kernel_panic
b *kmain

source utils.gdb

continue
