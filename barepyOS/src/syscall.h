#ifndef __syscall_h__
#define __syscall_h__
#include <stdint.h>
//Partie 1 : reboot
void sys_reboot();
void do_sys_reboot();
void sys_settime(uint64_t date_ms);
void swi_handler(void);
//Partie 2
void sys_nop(void);
void do_sys_nop(void);
//Partie 3
void sys_settime(uint64_t date_ms);
void do_sys_settime(void * stack_pointer);
//Partie 4 
uint64_t sys_gettime();
void do_sys_gettime();

#endif /* #ifndef __syscall_h__ */
