#include "syscall.h"
#include "util.h" //Pour Kernel Panic
#include "asm_tools.h" //Pour Set 32 
#include "hw.h"

void sys_reboot(){
	//__asm("mov %0, r0 " : "= r " ( va r0 ));
	__asm("mov r0, #1 ");
	__asm("SWI #0");
}

void sys_nop(){
	//Paramètre le lancement du swi_handler (voir les if imbriqués)
	__asm("mov r0, #2 ");
	__asm("SWI #0");
}

void sys_settime(uint64_t date_ms){
	//TO DEL : Récupération de la valeur en paramètre, pour la ranger dans R1 et R2 (car si on le met dans R2, 
	//TO DEL : __asm("mov r2,%0 " : "= r " ( date_ms ));
	
	// Consigne initiale : Placer valeur nécessaire dans la stack puis SWI
	//date_ms est dans r0 et r1 après cette commande. Mais on doit libérer r0 pour mettre le code d'appel.
	//On décale donc r1 dnas r2 et r0 dans r1
	__asm("mov r2, r1 ");
	__asm("mov r1, r0 ");
	
	//Paramètre le lancement du swi_handler (voir les if imbriqués)
	__asm("mov r0, #3 ");
	__asm("SWI #0");
}

uint64_t sys_gettime() {
	//On demande la récupération dans la stack de R1 et R2
	__asm("mov r0, #4 ");
	__asm("SWI #0");
	
	//uint32_t lowerbits = *((uint32_t*)(stack_pointer));
	uint32_t lowerbits;
	uint32_t higherbits;
	
	__asm("mov %0,r1 " : "= r " ( lowerbits )); //Les low bits dans R1
  	__asm("mov %0,r2 " : "= r " ( higherbits )); //Les high bits dans R2
  	
  	//On reconstruit
  	uint64_t date_ms = higherbits; // Reconstruction de la date
	date_ms = date_ms << 32; //Rolling barrel à gauche
	date_ms |= lowerbits; //Ou logique pour concaténer
	
	return date_ms;
}

void do_sys_settime(void * stack_pointer){
	/* Useless computation, as asked
	int x = 5;
	int y = 2;
	int z = 0;
	z = x*x+y;
	x = y*2+z;*/
	
	//Use the parameter (location of the stack pointer) to find the paramter in memory 
	
	// ETAPE 1 : récupération du uint64
	//On sait que le stack_pointer pointe sur le r0 des registres sauvegardés au début du swi_handler (peu importe ce qu'il s'est passé ensuite)
	//Par convention, la valeur du uint64 est répartie entre r1 (lowbits) et r2 (highbits)
	stack_pointer += 4; //32bits = 4 octets
	uint32_t lowerbits = *((uint32_t*)(stack_pointer)); 
	stack_pointer += 4; 
	uint32_t higherbits = *((uint32_t*)(stack_pointer));
	
	// ETAPE 2 : reconstruction de la date
	int64_t date_ms = higherbits;//On met les bits hauts dans une variable.
	date_ms = date_ms << 32; //Rolling barrel de 32 à gauche
	date_ms |= lowerbits; //"Ou binaire" pour coller les lowbits là où il n'y a pas les highbits, et garder les highbits

	//uint64_t date_ms;
	//__asm("mov %0,r2 " : "= r " ( date_ms ));

	set_date_ms(date_ms);
}

void do_sys_gettime(void * stack_pointer) {
	//On récupère la date
	uint64_t date_ms = get_date_ms(); //voir hw.h
	
	//On scinde en deux morceaux : les low bits et les highbits, 
	uint32_t date_lowbits = date_ms & (0x0000FFFF); //On récupère les bits de poids faible
  	uint32_t date_highbits = date_ms >> 32; //Rolling barrel à droite (la gauche est remplie de zéros)
  	
  	// SP pointe actuellement sur R0, on se place dans R1 : 
  	/* */
  	stack_pointer += 4; //32bits = 4 octets
  	*((uint32_t*)(stack_pointer)) = date_lowbits; //Les low bits dans R1 //écrasmeent de valeur sauvegardées de l'user space ? pas bien ?
	stack_pointer += 4; 
  	*((uint32_t*)(stack_pointer)) = date_highbits; //Les high bits dans R2
  	
  	//Note : Ces registres en stack seront rechargés en registres, et pourront être utilisés
  	
  	return;
}

void do_sys_nop(void){
	return;
}

void do_sys_reboot(){
	// Reconfiguration du Watchdog pour "éteindre les périphériques" avant le reboot
	//DEBUG : Le QEMU n'émule pas le watchdog. On "fait semblant" en sautant à l'adresse 0 pour simuler un reboot.
	__asm("b 0");
	
	/* Valide si c'est un vrai Raspberry Pi
	const int PM_RSTC = 0x2010001c;
	const int PM_WDOG = 0x20100024;
	const int PM_PASSWORD = 0x5a000000 ;
	const int PM_RSTC_WRCFG_FULL_RESET = 0x00000020 ;
	Set32 (PM_WDOG, PM_PASSWORD | 1 ) ;
	Set32 (PM_RSTC, PM_PASSWORD | PM_RSTC_WRCFG_FULL_RESET ) ;
	//Attente du signal 'Stop CPU" de la part du watchdog
	while( 1 ) ;
	*/
}


//On déclare une fonction avec un attribut "naked" pour éviter la généraiton 
//de prologue et d'épilogue de fonction, qui touchent au SP (en pushant automatiquement le LR, etc.)
//Mais qui ne fonctionne pas bien dans notre cas, car on sort de la fonction sans l'épilogue (grâce à un appel ASM)
__attribute__((naked)) void swi_handler(void);


void swi_handler(void){
	//sauvegarde des registres utilisateurs
	// ==== SOLUTION 1 : Passer en mode système, save les registres, passer en superviseur ==== 
	//__asm("cps 0b11111"); //0x pour hexa, 0b pour binaire
	//__asm("STMFD sp!, {r0-r12,lr}");
 	//__asm("cps 0b10011");
 	
 	//Pouser tous les registres sur la stack SVC
 	//Mémoriser la location du stack pointer
 	//Passer la location du stackpointer à la fonction do_sys_machin
 	
 	// ==== SOLUTION 2 : LR pas modifié par le noyau, pas besoin de sauvegarder. (normalement ?) ==== 
 	__asm("STMFD sp!, {r0-r12,lr}");
  	// ! Ici, car interruption, on est en mode superviseur. Donc le LR n'est pas le LR du mode utilisateur ! (vrai ? )
  	
  	void * stack_pointer;
	// On sauvegarde le pointeur de pile apres empilement des registres
	__asm("mov %0, sp" : "=r"(stack_pointer));
	
	//Récupération de la valeur du r0 
	int valr0 = 0;
	__asm("mov %0, r0 " : "= r " ( valr0 ));
	if(valr0==1){
		do_sys_reboot();
	} else if(valr0==2){
		do_sys_nop();
	} else if(valr0==3){
		//On donne en paramètre le pointeur de pile
		do_sys_settime(stack_pointer);
	} else if(valr0==4){
		//uint64_t systime = 
		do_sys_gettime(stack_pointer);
		//do_sys_gettime();
		//printf("%" PRIu64 "\n", systime);
	} else {
		//Lancement de kernel panic (défini dans util.h)
		PANIC();
	}
	
	// ==== SOLUTION 1 - part 2 : Passer en mode système, recharger les registres, passer en superviseur  ==== 
	//__asm("cps 0b11111"); //0x pour hexa, 0b pour binaire
	//__asm("LDMFD sp!, {r0-r12,pc}");
 	//__asm("cps 0b10011");
 	
 	 // ====  SOLUTION 2 : "^" à la fin de l'instruction indique de restorer le contenu de SPSR dans CPSR.  Ce qui fait changer de mode d'exécution donc de pointeur de pile (SP)  ==== 
 	__asm("LDMFD sp!, {r0-r12,pc}^");
	
	return;
}


