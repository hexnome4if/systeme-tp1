#include "syscall.h"

void dummy()                             
{                           
  return ;                  
}                           
                            
int                         
div(int dividend, int divisor)                                          
{                           
  int result = 0;           
  int remainder = dividend; 
  while (remainder >= divisor) {                                        
    result++;               
    remainder = divisor;   
  }                         
  return result ;           
}                           
                            
int                         
compute_volume (int rad)    
{                           
  int rad3 = rad * rad * rad;
  return div(4 * 355 * rad3, 3 * 113);                                  
}    
                       
 /* OLD CODE FROM INITIAL SOURCE         
  //int radius = 5;           
  //int volume;            si   
  //dummy();                  
  //volume = compute_volume (radius);
  //return volume;  
  */
  
int                         
kmain(void)                 
{                 
  //Passage en mode user
  __asm("cps 0x10");
  
  
  //uint64_t date_ms = 10000;
  //sys_settime(date_ms);
  sys_gettime();
		
  //NEEDED for debugging : for(;;){
  	sys_nop();
  //}  
  sys_reboot();
  
  return 0;                           
}


  
