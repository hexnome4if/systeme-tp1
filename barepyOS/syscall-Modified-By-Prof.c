#include "syscall.h"
#include "util.h" //Pour Kernel Panic
#include "asm_tools.h" //Pour Set 32 

void sys_reboot(){
    //__asm("mov %0, r0 " : "= r " ( va r0 ));
	__asm("mov r0, #1 ");
	__asm("SWI #0");
}

void sys_nop(){
	//ParamÃ¨tre le lancement du swi_handler (voir les if imbriquÃ©s)
	__asm("mov r0, #2 ");
	__asm("SWI #0");
}

void do_sys_reboot(){
	// =D 
	// Reconfiguration du Watchdog pour "Ã©teindre les pÃ©riphÃ©riques" avant le reboot
	//DEBUG : Le QEMU n'Ã©mule pas le watchdog. On "fait semblant" en sautant Ã  l'adresse 0 pour simuler un reboot.
	__asm("b 0");
	
	const int PM_RSTC = 0x2010001c;
	const int PM_WDOG = 0x20100024;
	const int PM_PASSWORD = 0x5a000000 ;
	const int PM_RSTC_WRCFG_FULL_RESET = 0x00000020 ;
	Set32 (PM_WDOG, PM_PASSWORD | 1 ) ;
	Set32 (PM_RSTC, PM_PASSWORD | PM_RSTC_WRCFG_FULL_RESET ) ;
	//Attente du signal 'Stop CPU" de la part du watchdog
	while( 1 ) ;
}

void do_sys_nop(void){
	return;
}

void swi_handler(void)
{
    //sauvegarde des registres utilisateurs
    
    __asm("STMFD sp!, {r0-r12,lr}");
    
    int valr0 = 0;

    //Récupération de la valeur du r0 
    __asm("mov %0, r0 " : "= r " ( valr0 ));

	//Si valeur attendu, on lance...
	if(valr0==1){
		// ...le reboot
		do_sys_reboot();
	}else if(valr0==2){
		do_sys_nop();
	}else if(valr0==3){
		PANIC();
	}else if(valr0==4){
		PANIC();
	} else {
	//Lancement de kernel panic (dÃ©fini dans util.h)
		PANIC();
	}
	__asm("LDMFD sp!, {r0-r12,pc}^");
	
	return;
}
